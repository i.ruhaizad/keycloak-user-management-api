var express = require("express");
const app = express();
var unirest = require("unirest");
var session = require('express-session');
var Keycloak = require('keycloak-connect');

var memoryStore = new session.MemoryStore();

app.use(session({
  secret: 'mySecret',
  resave: false,
  saveUninitialized: true,
  store: memoryStore
}));

var keycloak = new Keycloak({
  store: memoryStore
});

app.use(keycloak.middleware({
  logout: '/logout',
  admin: '/'
}));


var result = [];
var req = unirest("POST", "http://192.168.2.129:8080/auth/realms/master/protocol/openid-connect/token");

app.set('view engine', 'ejs');

req.headers({
  "Content-Type": "application/x-www-form-urlencoded"
});

req.form({
  "grant_type": "password",
  "username": "admin",
  "password": "Pa55w0rd",
  "client_id": "admin-cli"
});

req.end(function (res) {
  if (res.error) throw new Error(res.error);

  x = res.body.access_token;

  var req = unirest("GET", "http://192.168.2.129:8080/auth/admin/realms/CAMPAIGN_REALM/users");

  req.headers({
    "Authorization": "Bearer " + x
  });


  req.end(function (res) {
    if (res.error) throw new Error(res.error);
    result = res.body;
    for (var i = 0; i < result.length; i++) {
      console.log(result[i]["attributes"]);
    }
  });
});



app.get('/user', keycloak.protect(), function(req,res){
  res.render('user.ejs', {result: result});
});

app.get('/', function(req,res){
  res.render('home.ejs', {result: result});
});

// app.get('/home',function(req,res){
//   res.render('home.ejs')
// })

app.listen(5000, () => console.log('Example app listening on port 5000!'))
